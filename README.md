# Data Structures for N-Tier Applications

N-Tier is a library containing common data structures and classes used in
making the domain layer of n-tier web applications.

Please note: documentation on this project is currently not provided

## Interacting With the Project

`make` is the best way to interact with the project.

### Setting Up:

    make install

### Running Tests:

    make test

### Running Coverage

    make coverage
