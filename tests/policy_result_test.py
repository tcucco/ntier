# pylint: disable=missing-docstring
from unittest import TestCase

from ntier.policy_result import PolicyResult


class PolicyResultTest(TestCase):
    def test_bool(self):
        self.assertTrue(PolicyResult.success())
        self.assertFalse(PolicyResult.failed("not authorized"))

    def test_success(self):
        pres = PolicyResult.success()
        self.assertTrue(pres.is_valid)
        self.assertEqual(pres.messages.data, {})

    def test_failed(self):
        pres = PolicyResult.failed("not authenticated")
        self.assertFalse(pres.is_valid)
        self.assertEqual(pres.messages.data, {"": ["not authenticated"]})

    def test_union(self):
        ok_1 = PolicyResult.success()
        ok_2 = PolicyResult.success()
        error_1 = PolicyResult.failed("you can't do that")
        error_2 = PolicyResult.failed("still can't do that")

        union_1 = ok_1.union(ok_2)
        self.assertTrue(union_1.is_valid)
        self.assertEqual(union_1.messages.data, {})

        union_2 = ok_1.union(error_1)
        self.assertFalse(union_2.is_valid)
        self.assertEqual(union_2.messages.data, {"": ["you can't do that"]})

        union_3 = error_1.union(error_2)
        self.assertFalse(union_3.is_valid)
        self.assertEqual(
            union_3.messages.data, {"": ["you can't do that", "still can't do that"]}
        )
