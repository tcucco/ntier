# pylint: disable=missing-docstring
import asyncio
from typing import Any, Mapping, MutableMapping, Optional
from unittest import TestCase

from ntier.policy_result import PolicyResult
from ntier.transaction_base import TransactionBase
from ntier.transaction_code import TransactionCode
from ntier.transaction_result import TransactionResult
from ntier.validation_result import ValidationResult


class TransactionBaseTest(TestCase):
    def test_not_authenticated(self):
        trans = Transaction("authenticate")
        tres = asyncio.run(trans({}))
        self.assertEqual(tres.status_code, TransactionCode.not_authenticated)

    def test_not_found(self):
        trans = Transaction("find")
        tres = asyncio.run(trans({}))
        self.assertEqual(tres.status_code, TransactionCode.not_found)

    def test_not_authorized(self):
        trans = Transaction("authorize")
        tres = asyncio.run(trans({}))
        self.assertEqual(tres.status_code, TransactionCode.not_authorized)

    def test_not_valid(self):
        trans = Transaction("validate")
        tres = asyncio.run(trans({}))
        self.assertEqual(tres.status_code, TransactionCode.not_valid)

    def test_success(self):
        trans = Transaction()
        tres = asyncio.run(trans({}))
        self.assertEqual(tres.status_code, TransactionCode.success)


InputType = Mapping[str, Any]
StateType = MutableMapping[str, Any]


class Transaction(TransactionBase[InputType, StateType]):
    def __init__(self, fail_at: str = "") -> None:
        super().__init__()
        self.fail_at = fail_at

    async def map_input(self, data: InputType) -> StateType:
        return {**data}

    async def authenticate(self, state: StateType) -> bool:
        if self.fail_at == "authenticate":
            return False
        return await super().authenticate(state)

    async def find(self, state: StateType) -> Optional[str]:
        if self.fail_at == "find":
            return "entity"
        return await super().find(state)

    async def authorize(self, state: StateType) -> PolicyResult:
        if self.fail_at == "authorize":
            return PolicyResult.failed("not allowed")
        return await super().authorize(state)

    async def validate(self, state: StateType) -> ValidationResult:
        if self.fail_at == "validate":
            return ValidationResult.builder().add_message("name", "is required")
        return await super().validate(state)

    async def perform(self, state: StateType) -> TransactionResult:
        return TransactionResult.success()
