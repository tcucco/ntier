# pylint: disable=missing-docstring,no-self-use

import datetime
import decimal
import uuid
from dataclasses import dataclass, field
from typing import Dict
from unittest import TestCase

from ntier.deserialize import Deserialize as D
from ntier.deserialize import MapDef
from ntier.deserialize_html import default_html_stripper, naive_html_stripper


class DeserializeTest(TestCase):
    def test_int(self):
        self.assertEqual(D.int("5"), 5)
        self.assertEqual(D.int(10), 10)

        self.assertIsNone(D.int("a"))
        self.assertIsNone(D.int(None))
        self.assertIsNone(D.int(""))
        self.assertIsNone(D.int([]))
        self.assertIsNone(D.int({"a": 1}))
        self.assertIsNone(D.int("5.4"))

    def test_float(self):
        self.assertEqual(D.float("5"), 5)
        self.assertEqual(D.float(10), 10)
        self.assertEqual(D.float("5.4"), 5.4)
        self.assertEqual(D.float("-5.6"), -5.6)

        self.assertIsNone(D.float("a"))
        self.assertIsNone(D.float(None))
        self.assertIsNone(D.float(""))
        self.assertIsNone(D.float([]))
        self.assertIsNone(D.float({"a": 1}))

    def test_date(self):
        self.assertEqual(D.date("2020-09-18"), datetime.date(2020, 9, 18))

        self.assertIsNone(D.date(None))
        self.assertIsNone(D.date(""))
        self.assertIsNone(D.date("September 19th, 2020"))

    def test_datetime(self):
        self.assertEqual(
            D.datetime("2020-09-18T14:30:00"), datetime.datetime(2020, 9, 18, 14, 30, 0)
        )
        self.assertEqual(
            D.datetime("2020-09-18T14:30:00-06:00"),
            datetime.datetime(
                2020,
                9,
                18,
                14,
                30,
                0,
                0,
                datetime.timezone(datetime.timedelta(hours=-6)),
            ),
        )
        self.assertIsNone(D.datetime(None))

    def test_bool(self):
        self.assertEqual(D.bool("T"), True)
        self.assertEqual(D.bool("TRUE"), True)
        self.assertEqual(D.bool("yes"), True)
        self.assertEqual(D.bool("Y"), True)
        self.assertEqual(D.bool(1), True)

        self.assertEqual(D.bool(0), False)
        self.assertEqual(D.bool("no"), False)
        self.assertEqual(D.bool("False"), False)
        self.assertEqual(D.bool("f"), False)

        self.assertEqual(D.bool(""), None)

    def test_uuid(self):
        self.assertEqual(
            D.uuid("123e4567-e89b-12d3-a456-426614174000"),
            uuid.UUID("123e4567-e89b-12d3-a456-426614174000"),
        )
        self.assertIsNone(D.uuid("123e4567-e89b-12d3-a456"))
        self.assertIsNone(D.uuid("123"))
        self.assertIsNone(D.uuid("abcdefghijk"))
        self.assertIsNone(D.uuid(""))
        self.assertIsNone(D.uuid(None))

        uuid_val = uuid.uuid4()
        self.assertEqual(
            D.uuid(uuid_val),
            uuid_val,
        )

    def test_decimal(self):
        self.assertEqual(D.decimal("123.45"), decimal.Decimal("123.45"))
        self.assertIsNone(D.decimal("a"))
        self.assertIsNone(D.decimal(""))
        self.assertIsNone(D.decimal(None))
        self.assertIsNone(D.decimal({}))

    def test_base64(self):
        self.assertEqual(D.base64("c2V2ZXJ1cyBzbmFwZQ=="), b"severus snape")
        self.assertEqual(D.base64(b"c2V2ZXJ1cyBzbmFwZQ=="), b"severus snape")
        self.assertIsNone(D.base64(""))
        self.assertIsNone(D.base64(None))
        self.assertIsNone(D.base64([]))

    def test_list(self):
        fn = D.list(D.int)

        self.assertEqual(fn(["1", "2", 3, "4", 5]), [1, 2, 3, 4, 5])
        self.assertEqual(fn([]), [])
        self.assertEqual(fn(None), [])
        self.assertEqual(fn({}), [])

    def test_container_no_data(self):
        self.assertEqual(D.map({"id": D.int})(None), {})
        self.assertEqual(D.list(D.int)(None), [])

    def test_map_dict(self):
        person = {"name": D.html_safe_text(), "age": D.int, "height": D.float}
        parent = {**person, "children": D.list(D.map(person))}
        fn = D.map(parent)

        parents = {
            "name": "<h1>Harry Potter</h1>",
            "age": "36",
            "height": "5.92",
            "children": [
                {"name": "Albus Severus Potter", "age": "11"},
                {"name": "James Sirius Potter", "age": "13"},
            ],
        }
        expected = {
            "name": "Harry Potter",
            "age": 36,
            "height": 5.92,
            "children": [
                {"name": "Albus Severus Potter", "age": 11, "height": None},
                {"name": "James Sirius Potter", "age": 13, "height": None},
            ],
        }

        self.assertEqual(fn(parents), expected)

    def test_map(self):
        data = {
            "url_args": {"user_id": "3"},
            "query_args": {"page": "1"},
            "body": {
                "name": "Severus Snape",
                "age": "40",
                "house": "Slytherin",
            },
        }
        expected = {
            "user_id": 3,
            "page": 1,
            "user": {
                "name": "Severus Snape",
                "age": 40,
                "house": "Slytherin",
            },
        }
        fn = D.map(
            [
                MapDef(lambda v: v["url_args"]["user_id"], D.int, "user_id"),
                MapDef(lambda v: v["query_args"]["page"], D.int, "page"),
                MapDef(
                    lambda v: v["body"],
                    D.map({"name": D.text, "age": D.int, "house": D.text}),
                    "user",
                ),
            ]
        )
        self.assertEqual(fn(data), expected)

    def test_text(self):
        self.assertEqual(D.text("  a "), "a")
        self.assertEqual(D.text(None), None)
        self.assertEqual(D.text("  "), None)
        self.assertEqual(D.text(""), None)
        self.assertEqual(D.text("hello, world!"), "hello, world!")

    def test_html_safe_text_no_exceptions(self):
        D.register_html_stripper(naive_html_stripper)
        fn = D.html_safe_text()
        self.assertEqual(fn("hello"), "hello")
        self.assertEqual(fn("    hello"), "hello")
        self.assertEqual(fn(" hello  "), "hello")

        self.assertIsNone(fn("    "))
        self.assertIsNone(fn(None))

        self.assertEqual(fn("<script>alert('hello!')</script>"), "alert('hello!')")
        self.assertEqual(
            fn("<h1><b>Hello!</b> <i>how</i> <u>are</u> <s>you?</s></h1>"),
            "Hello! how are you?",
        )

    def test_html_safe_text(self):
        D.register_html_stripper(default_html_stripper)
        html = (
            '<p onhover="alert(hello)">Hello there <script>alert("hello")</script> good <b>'
            "friend</b></p>"
        )
        expected = '<p>Hello there alert("hello") good <b>friend</b></p>'
        fn = D.html_safe_text(tags=["p", "b", "i"], attributes={})
        self.assertEqual(fn(html), expected)

    def test_key_present(self):
        map_def = D.key("age", D.int)
        value = map_def.map(map_def.extract({"age": "13"}))
        self.assertEqual(value, 13)

    def test_key_missing(self):
        map_def = D.key("age", D.int, fallback=-1)
        value = map_def.map(map_def.extract({}))
        self.assertEqual(value, -1)

    def test_path_present(self):
        map_def = D.path(["person", "age"], D.int, "age")
        value = map_def.map(map_def.extract({"person": {"name": "Snape", "age": "34"}}))
        self.assertEqual(value, 34)

    def test_path_missing_1(self):
        map_def = D.path(["person", "age"], D.int, "age", -1)
        value = map_def.map(map_def.extract({"person": {"name": "Snape"}}))
        self.assertEqual(value, -1)

    def test_path_missing_2(self):
        map_def = D.path(["person", "age"], D.int, "age", -1)
        value = map_def.map(map_def.extract({}))
        self.assertEqual(value, -1)

    def test_path_attr(self):
        person = Person(
            "Trey",
            "Cucco",
            38,
            {
                "Bob": Person("Bob", "Jones", 36),
                "Alice": Person("Alice", "Roberts", 34),
            },
        )
        map_def = D.path(
            ["person", D.attr("friends"), "Bob", D.attr("last_name")], D.text
        )
        value = map_def.map(map_def.extract({"person": person}))
        self.assertEqual(value, "Jones")

    def test_map_missing_3(self):
        extract = D.map(
            [
                D.path(["person", "first_name"], D.text),
                D.path(["person", "last_name"], D.text),
                D.path(["person", "age"], D.int),
            ]
        )
        value = extract(
            {
                "person": {
                    "first_name": "Trey",
                    "last_name": "Cucco",
                },
            }
        )
        self.assertEqual(value, {"first_name": "Trey", "last_name": "Cucco"})


@dataclass
class Person:
    first_name: str
    last_name: str
    age: int
    friends: Dict[str, "Person"] = field(default_factory=dict)
