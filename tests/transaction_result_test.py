# pylint: disable=missing-docstring
from unittest import TestCase

from ntier.paging import Paging
from ntier.policy_result import PolicyResult
from ntier.transaction_code import TransactionCode
from ntier.transaction_result import TransactionResult
from ntier.validation_result import ValidationResult


class TransactionResultTest(TestCase):
    def test_repr(self):
        val = repr(TransactionResult.found())
        self.assertEqual(
            val,
            "TransactionResult(is_valid=True, status_code=TransactionCode.found, payload=None, paging=None)",
        )

    def test_has_paging(self):
        tres = TransactionResult.found(
            {"users": [{"id": 1, "name": "Trey"}]},
        )
        self.assertFalse(tres.has_paging)
        tres.set_paging(Paging(1, 25))
        self.assertTrue(tres.has_paging)

    def test_set_paging(self):
        tres = TransactionResult.success()
        tres.set_paging(Paging(1, 25, 100))
        self.assertEqual(tres.paging.page, 1)
        self.assertEqual(tres.paging.per_page, 25)
        self.assertEqual(tres.paging.total_records, 100)

    def test_paging_not_set(self):
        tres = TransactionResult.success()
        with self.assertRaises(Exception):
            _ = tres.paging

    def test_success(self):
        tres = TransactionResult.success()
        self.assertTrue(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.success)

    def test_found(self):
        tres = TransactionResult.found()
        self.assertTrue(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.found)

    def test_created(self):
        tres = TransactionResult.created()
        self.assertTrue(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.created)

    def test_updated(self):
        tres = TransactionResult.updated()
        self.assertTrue(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.updated)

    def test_not_changed(self):
        tres = TransactionResult.not_changed()
        self.assertTrue(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.not_changed)

    def test_deleted(self):
        tres = TransactionResult.deleted()
        self.assertTrue(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.deleted)

    def test_failed(self):
        tres = TransactionResult.failed()
        self.assertFalse(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.failed)

    def test_not_found(self):
        tres = TransactionResult.not_found("user")
        self.assertFalse(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.not_found)
        self.assertEqual(tres.payload, {"": ["user not found"]})

    def test_not_authenticated(self):
        tres = TransactionResult.not_authenticated()
        self.assertFalse(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.not_authenticated)

    def test_not_authorized(self):
        tres = TransactionResult.not_authorized(PolicyResult.failed("not allowed"))
        self.assertFalse(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.not_authorized)
        self.assertEqual(tres.payload, {"": ["not allowed"]})

    def test_not_valid(self):
        vres = ValidationResult.builder().add_message("name", "is required")
        tres = TransactionResult.not_valid(vres)
        self.assertFalse(tres.is_valid)
        self.assertEqual(tres.status_code, TransactionCode.not_valid)
