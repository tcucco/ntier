# pylint: disable=missing-docstring
from unittest import TestCase

from ntier.paging import Paging


class PagingTest(TestCase):
    def test_build(self):
        paging = Paging(1, 25)
        paging.set_total_records(98)
        self.assertEqual(paging.page, 1)
        self.assertEqual(paging.per_page, 25)
        self.assertEqual(paging.total_records, 98)
        self.assertEqual(paging.total_pages, 4)
        self.assertEqual(paging.limit, 25)
        self.assertEqual(paging.offset, 0)

    def test_parse(self):
        paging_1 = Paging.parse(None, None)
        self.assertEqual((paging_1.page, paging_1.per_page), (1, 25))

        paging_2 = Paging.parse("1", "40")
        self.assertEqual((paging_2.page, paging_2.per_page), (1, 40))

        paging_3 = Paging.parse("two", "fifty-three")
        self.assertEqual((paging_3.page, paging_3.per_page), (1, 25))

    def test_default(self):
        paging = Paging.default()
        self.assertEqual(paging.page, 1)
        self.assertEqual(paging.per_page, 25)
