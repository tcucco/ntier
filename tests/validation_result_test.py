# pylint: disable=missing-docstring
from unittest import TestCase

from ntier.messages import Messages
from ntier.validation_result import ValidationResult


class ValidationResultTest(TestCase):
    def test_bool(self):
        self.assertTrue(ValidationResult.success())
        self.assertFalse(
            ValidationResult.failed(Messages().add_message("name", "required"))
        )

    def test_is_valid(self):
        self.assertTrue(ValidationResult.success().is_valid)
        self.assertFalse(
            ValidationResult.failed(Messages().add_message("name", "required")).is_valid
        )

    def test_messages(self):
        self.assertEqual(ValidationResult().messages.data, {})
        self.assertEqual(
            ValidationResult(Messages().add_message("name", "required")).messages.data,
            {"name": ["required"]},
        )

    def test_add_message(self):
        vres = ValidationResult()
        self.assertTrue(vres.is_valid)
        vres.add_message("name", "required")
        self.assertFalse(vres.is_valid)
        self.assertEqual(vres.messages.data, {"name": ["required"]})

    def test_add_general_message(self):
        vres = ValidationResult()
        self.assertTrue(vres.is_valid)
        vres.add_general_message("not authorized")
        self.assertFalse(vres.is_valid)
        self.assertEqual(vres.messages.data, {"": ["not authorized"]})

    def test_success(self):
        vres = ValidationResult.success()
        self.assertTrue(vres.is_valid)

    def test_failed(self):
        msg = Messages().add_message("name", "is required")
        vres = ValidationResult.failed(msg)
        self.assertFalse(vres.is_valid)
        self.assertEqual(vres.messages.data, {"name": ["is required"]})

    def test_builder(self):
        vres = ValidationResult.builder()
        self.assertTrue(vres.is_valid)

    def test_union(self):
        ok_1 = ValidationResult()
        ok_2 = ValidationResult()
        error_1 = ValidationResult().add_message("first_name", "must be present")
        error_2 = ValidationResult().add_message("last_name", "must be present")

        union_1 = ok_1.union(ok_2)
        self.assertTrue(union_1.is_valid)
        self.assertEqual(union_1.messages.data, {})

        union_2 = ok_1.union(error_1)
        self.assertFalse(union_2.is_valid)
        self.assertEqual(union_2.messages.data, {"first_name": ["must be present"]})

        union_3 = error_1.union(error_2)
        self.assertFalse(union_3.is_valid)
        self.assertEqual(
            union_3.messages.data,
            {"first_name": ["must be present"], "last_name": ["must be present"]},
        )
