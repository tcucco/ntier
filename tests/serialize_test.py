# pylint: disable=missing-docstring
from base64 import b64encode
from datetime import date, datetime, timezone
from decimal import Decimal
from unittest import TestCase
from uuid import uuid4

from ntier.serialize import Serialize as S


class SerializeTest(TestCase):
    def test_map(self):
        serialize = S.map(lambda d: d["id"])
        items = [
            {"id": 1},
            {"id": 2},
            {"id": 3},
        ]
        serialized = serialize(items)
        self.assertEqual(serialized, [1, 2, 3])

    def test_pick(self):
        serialize = S.pick(["id", "name"])
        item = {"id": 1, "name": "Trey", "age": 35}
        serialized = serialize(item)
        self.assertEqual(serialized, {"id": 1, "name": "Trey"})

    def test_exclude(self):
        serialize = S.exclude(["age"])
        item = {"id": 1, "name": "Trey", "age": 35}
        serialized = serialize(item)
        self.assertEqual(serialized, {"id": 1, "name": "Trey"})

    def test_evolve(self):
        serialize = S.evolve({"age": lambda v: v - 5})
        item = {"id": 1, "name": "Trey", "age": 35}
        serialized = serialize(item)
        self.assertEqual(serialized, {"id": 1, "name": "Trey", "age": 30})

    def test_evolve_strict(self):
        serialize = S.evolve({"age": lambda v: v - 5}, strict=True)
        item = {"id": 1, "name": "Trey", "age": 35}
        serialized = serialize(item)
        self.assertEqual(serialized, {"age": 30})

    def test_compose(self):
        serialize = S.compose(
            S.evolve({"age": lambda v: v - 5}),
            S.pick(["name", "age"]),
        )
        item = {"id": 1, "name": "Trey", "age": 35}
        serialized = serialize(item)
        self.assertEqual(serialized, {"name": "Trey", "age": 30})

    def test_optional(self):
        serialize = S.optional(lambda v: v * 2)
        self.assertEqual(serialize(4), 8)
        self.assertEqual(serialize(None), None)

    def test_date(self):
        val = date.today()
        self.assertEqual(S.date(val), val.isoformat())

    def test_datetime(self):
        val = datetime.now(timezone.utc)
        self.assertEqual(S.datetime(val), val.isoformat(timespec="microseconds"))

        start_of_day = val.replace(microsecond=0)
        self.assertEqual(
            S.datetime(start_of_day), start_of_day.isoformat(timespec="microseconds")
        )

    def test_uuid(self):
        val = uuid4()
        self.assertEqual(S.uuid(val), str(val))

    def test_decimal(self):
        val = Decimal("3.14")
        self.assertEqual(S.decimal(val), "3.14")

    def test_bytes(self):
        val = b"tom marvolo riddle == lord voldemort"
        self.assertEqual(S.bytes(val), b64encode(val).decode("utf-8"))
