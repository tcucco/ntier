# pylint: disable=missing-docstring
from unittest import TestCase

from ntier.messages import Messages


class MessagesTest(TestCase):
    def test_bool(self):
        msg = Messages()
        self.assertFalse(msg)
        msg.add_message("name", "must be present")
        self.assertTrue(msg)

    def test_has_messages(self):
        msg = Messages()
        self.assertFalse(msg)
        msg.add_message("name", "must be present")
        self.assertTrue(msg)

    def test_messages(self):
        msg = Messages()
        msg.add_message("name", "must be present")
        msg.add_message("name", "must be 20 characters or less")
        msg.add_general_message("must be authenticated")
        self.assertEqual(
            msg.data,
            {
                "": ["must be authenticated"],
                "name": [
                    "must be present",
                    "must be 20 characters or less",
                ],
            },
        )

    def test_add_message(self):
        msg = Messages()
        msg.add_message("name", "must be present")
        msg.add_message("name", "must be 20 characters or less")
        self.assertEqual(
            msg.data,
            {
                "name": [
                    "must be present",
                    "must be 20 characters or less",
                ],
            },
        )

    def test_add_general_message(self):
        msg = Messages()
        msg.add_general_message("not authenticated")
        self.assertEqual(msg.data, {"": ["not authenticated"]})

    def test_has_message_for(self):
        msg = Messages()
        msg.add_message("name", "must be present")
        self.assertTrue(msg.has_message_for("name"))
        self.assertFalse(msg.has_message_for("password"))

    def test_has_general_message(self):
        msg = Messages()
        self.assertFalse(msg.has_general_message())
        msg.add_general_message("not authenticated")
        self.assertTrue(msg.has_general_message())

    def test_merge(self):
        msg = Messages()
        msg.add_message("first_name", "must be present")
        msg.add_message("last_name", "must be present")
        other = Messages()
        other.add_message("first_name", "must be at least 3 characters long")
        other.add_message("middle_initial", "must be 0 or 1 characters long")
        msg.merge(other.data)
        self.assertEqual(
            msg.data,
            {
                "first_name": ["must be present", "must be at least 3 characters long"],
                "last_name": ["must be present"],
                "middle_initial": ["must be 0 or 1 characters long"],
            },
        )

    def test_union(self):
        msg = Messages()
        msg.add_message("first_name", "must be present")
        msg.add_message("last_name", "must be present")
        other = Messages()
        other.add_message("first_name", "must be at least 3 characters long")
        other.add_message("middle_initial", "must be 0 or 1 characters long")
        union = msg.union(other)

        self.assertTrue(msg is not union)
        self.assertTrue(other is not union)

        self.assertEqual(
            union.data,
            {
                "first_name": ["must be present", "must be at least 3 characters long"],
                "last_name": ["must be present"],
                "middle_initial": ["must be 0 or 1 characters long"],
            },
        )
