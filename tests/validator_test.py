# pylint: disable=missing-docstring,no-self-use
import asyncio
import re
from datetime import datetime, timezone
from decimal import Decimal
from unittest import IsolatedAsyncioTestCase, TestCase

from ntier.validator import Validator, Validators


class ValidatorTest(TestCase):
    def get_validator(self):
        return Validator(
            [
                Validator.field("name", "must pass", self.validate_field),
                Validator.field(
                    "name_async", "must pass async", self.validate_field_async
                ),
                Validator.field("name", "must pass 2", self.validate_field),
                Validator.record("must pass", self.validate_record),
                Validator.record(
                    "must pass async", self.validate_record_async, "check_async"
                ),
                Validator.field(
                    "description", "must be present", Validators.is_present
                ),
                Validator.record(
                    "must be right length",
                    self.validate_description,
                    "description",
                    ["description"],
                ),
            ]
        )

    def validate_field(self, value):
        return value == "sync"

    async def validate_field_async(self, value):
        return value == "async"

    def validate_record(self, record):
        return record["check"] == "sync"

    async def validate_record_async(self, record):
        return record["check_async"] == "async"

    def validate_description(self, record):
        desc_len = len(record["description"])
        if desc_len < 5:
            return "must be {0} characters longer".format(5 - desc_len)
        if desc_len > 10:
            return "must be {0} characters shorter".format(desc_len - 10)
        return None

    def validate(self, data, validator=None, *, fail_fast=False):
        if not validator:
            validator = self.get_validator()
        return asyncio.run(validator.validate(data, fail_fast=fail_fast))

    def test_bad_validator(self):
        validator = Validator([None])
        with self.assertRaises(Exception):
            self.validate({}, validator)

    def test_validate_no_data(self):
        vres = self.validate(None)
        self.assertFalse(vres)
        self.assertEqual(vres.messages.data, {"": ["no data"]})

    def test_validate_field(self):
        data = {
            "name": "",
        }
        vres = self.validate(data)
        self.assertFalse(vres)
        self.assertEqual(
            vres.messages.data,
            {
                "name": ["must pass"],
                "name_async": ["must pass async"],
                "description": ["must be present"],
            },
        )

    def test_validate_record(self):
        data = {
            "name": "sync",
            "name_async": "async",
            "check": "",
            "description": "abcde",
        }
        vres = self.validate(data)
        self.assertFalse(vres)
        self.assertEqual(vres.messages.data, {"": ["must pass"]})

    def test_validate_record_async(self):
        data = {
            "name": "sync",
            "name_async": "async",
            "check": "sync",
            "check_async": "",
            "description": "abcde",
        }
        vres = self.validate(data)
        self.assertFalse(vres)
        self.assertEqual(vres.messages.data, {"check_async": ["must pass async"]})

    def test_validate_ok(self):
        vres = self.validate(
            {
                "name": "sync",
                "name_async": "async",
                "check": "sync",
                "check_async": "async",
                "age": 35,
                "age_async": 36,
                "description": "abcdef",
            }
        )
        self.assertTrue(vres)

    def test_validate_record_when_dependencies_ok_even_if_other_errors(self):
        vres = self.validate({"description": "a"})
        self.assertFalse(vres)
        self.assertEqual(
            vres.messages.data,
            {
                "name": ["must pass"],
                "name_async": ["must pass async"],
                "description": ["must be 4 characters longer"],
            },
        )


class ValidatorsTest(IsolatedAsyncioTestCase):
    def test_is_present(self):
        self.assertTrue(Validators.is_present("a"))
        self.assertTrue(Validators.is_present(0))
        self.assertTrue(Validators.is_present(""))
        self.assertTrue(Validators.is_present({}))
        self.assertTrue(Validators.is_present([]))

        self.assertFalse(Validators.is_present(None))

    async def test_optional(self):
        validator = Validators.optional(Validators.is_match(r"^\w+$"))
        self.assertTrue(await validator(None))
        self.assertTrue(await validator("trey"))
        self.assertFalse(await validator("--#--"))

    async def test_any(self):
        fn = Validators.any([Validators.is_type(str), Validators.is_type(int)])

        self.assertTrue(await fn("3"))
        self.assertTrue(await fn(3))
        self.assertFalse(await fn([]))

    async def test_all(self):
        fn = Validators.all([Validators.is_type(str), Validators.is_not_empty])

        self.assertTrue(await fn("3"))
        self.assertFalse(await fn(""))
        self.assertFalse(await fn(["a", "b", "c"]))

    async def test_list(self):
        item_validator = Validator(
            [
                Validator.field("name", "must be present", Validators.is_present),
                Validator.field(
                    "age", "must be non-negative", Validators.is_greater_or_equal(0)
                ),
                Validator.field(
                    "color",
                    "must brown, blue, or green",
                    Validators.is_member({"brown", "blue", "green"}),
                ),
            ]
        )
        validator = Validator(
            [
                Validator.field(
                    "family", "must be valid people", Validators.list(item_validator)
                ),
            ]
        )

        self.assertTrue(
            await validator.validate(
                {
                    "family": [
                        {
                            "name": "Trey",
                            "age": 38,
                            "color": "blue",
                        },
                        {
                            "name": "Julie",
                            "age": 36,
                            "color": "green",
                        },
                    ]
                }
            )
        )

        self.assertFalse(
            await validator.validate(
                {
                    "family": [
                        {
                            "name": "Trey",
                            "age": 38,
                            "color": "blue",
                        },
                        {
                            "name": "Julie",
                            "age": 36,
                            "color": "YELLOW",
                        },
                    ]
                }
            )
        )

        self.assertTrue(await validator.validate({"family": []}))
        self.assertFalse(await validator.validate({"family": ["Trey", "Julie"]}))

    def test_is_type(self):
        self.assertTrue(Validators.is_type(str)("abc"))
        self.assertTrue(Validators.is_type(int)(3))
        self.assertTrue(Validators.is_type(float)(3.0))

        self.assertFalse(Validators.is_type(int)("3"))

        self.assertTrue(Validators.is_type((int, float))(3))
        self.assertTrue(Validators.is_type((int, float))(3.1))

    def test_is_list_type(self):
        self.assertTrue(Validators.is_list_type(str)(["a", "b", "c"]))
        self.assertTrue(Validators.is_list_type(str)(["1", "2", "3"]))
        self.assertTrue(Validators.is_list_type(int)([1, 2, 3]))
        self.assertFalse(Validators.is_list_type(int)(["1", "2", "3"]))
        self.assertFalse(Validators.is_list_type(str)(["a", None]))
        self.assertTrue(Validators.is_list_type(int)([]))
        self.assertFalse(Validators.is_list_type(int)(None))

    def test_is_member(self):
        validator = Validators.is_member({"UP", "DOWN", "LEFT", "RIGHT"})
        self.assertTrue(validator("UP"))
        self.assertFalse(validator("NORTHEAST"))

    def test_is_not_empty(self):
        validator = Validators.is_not_empty
        self.assertTrue(validator("hi"))
        self.assertFalse(validator(""))
        self.assertTrue(validator(" "))
        self.assertFalse(validator([]))
        self.assertTrue(validator([1]))
        self.assertFalse(validator({}))
        self.assertTrue(validator({"a": 1}))
        self.assertFalse(validator(set()))
        self.assertTrue(validator({1}))

    def test_is_match(self):
        pattern = r"^[0-9a-fA-F]{6}$"
        match_1 = Validators.is_match(pattern)
        match_2 = Validators.is_match(re.compile(pattern))

        self.assertFalse(match_1(None))
        self.assertFalse(match_2(None))

        self.assertFalse(match_1(""))
        self.assertFalse(match_2(""))

        self.assertFalse(match_1("aaff0"))
        self.assertFalse(match_2("aaff0"))

        self.assertTrue(match_1("aaff00"))
        self.assertTrue(match_2("aaff00"))

    def test_is_length(self):
        validator1 = Validators.is_length(5, None)
        validator2 = Validators.is_length(5, 10)
        validator3 = Validators.is_length(None, 10)
        values = [
            ("1111", False, False, True),
            ("11111", True, True, True),
            ("1111111", True, True, True),
            ("11111111111", True, False, False),
            (None, False, False, False),
        ]
        for (value, exp1, exp2, exp3) in values:
            self.assertEqual(validator1(value), exp1)
            self.assertEqual(validator2(value), exp2)
            self.assertEqual(validator3(value), exp3)

    def test_is_greater(self):
        fn = Validators.is_greater(5)

        self.assertTrue(fn(10))
        self.assertTrue(fn(7.5))
        self.assertTrue(fn(Decimal("5.5")))
        self.assertFalse(fn(5))
        self.assertFalse(fn(4))
        self.assertFalse(fn(3.5))
        self.assertFalse(fn(Decimal("2.5")))
        self.assertFalse(fn("a"))
        self.assertFalse(fn(None))

    def test_is_lesser(self):
        fn = Validators.is_lesser(5)

        self.assertFalse(fn(10))
        self.assertFalse(fn(7.5))
        self.assertFalse(fn(Decimal("5.5")))
        self.assertFalse(fn(5))
        self.assertTrue(fn(4))
        self.assertTrue(fn(3.5))
        self.assertTrue(fn(Decimal("2.5")))
        self.assertFalse(fn("a"))
        self.assertFalse(fn(None))

    def test_is_greater_or_equal(self):
        fn = Validators.is_greater_or_equal(5)

        self.assertTrue(fn(10))
        self.assertTrue(fn(7.5))
        self.assertTrue(fn(Decimal("5.5")))
        self.assertTrue(fn(5))
        self.assertFalse(fn(4))
        self.assertFalse(fn(3.5))
        self.assertFalse(fn(Decimal("2.5")))
        self.assertFalse(fn("a"))
        self.assertFalse(fn(None))

    def test_is_lesser_or_equal(self):
        fn = Validators.is_lesser_or_equal(5)

        self.assertFalse(fn(10))
        self.assertFalse(fn(7.5))
        self.assertFalse(fn(Decimal("5.5")))
        self.assertTrue(fn(5))
        self.assertTrue(fn(4))
        self.assertTrue(fn(3.5))
        self.assertTrue(fn(Decimal("2.5")))
        self.assertFalse(fn("a"))
        self.assertFalse(fn(None))

    def test_is_between(self):
        fn = Validators.is_between(5, 10)

        self.assertFalse(fn(12))
        self.assertTrue(fn(10))
        self.assertTrue(fn(7.5))
        self.assertTrue(fn(Decimal("5.5")))
        self.assertTrue(fn(5))
        self.assertFalse(fn(4))
        self.assertFalse(fn(3.5))
        self.assertFalse(fn("a"))
        self.assertFalse(fn(None))
        self.assertFalse(fn(Decimal("2.5")))

    def test_equals(self):
        fn = Validators.equals(5)
        self.assertTrue(fn(5))
        self.assertFalse(fn("5"))
        self.assertFalse(fn([5]))

        fn = Validators.equals("5")
        self.assertFalse(fn(5))
        self.assertTrue(fn("5"))
        self.assertFalse(fn([5]))

        fn = Validators.equals([5])
        self.assertFalse(fn(5))
        self.assertFalse(fn("5"))
        self.assertTrue(fn([5]))

    def test_has_timezone(self):
        fn = Validators.has_timezone

        self.assertFalse(fn(datetime.now()))
        self.assertTrue(fn(datetime.now(timezone.utc)))
        self.assertFalse(fn(datetime.fromisoformat("2020-11-09T15:54:00")))
        self.assertTrue(fn(datetime.fromisoformat("2020-11-09T15:54:00+00:00")))
        self.assertTrue(fn(datetime.fromisoformat("2020-11-09T15:54:00+06:00")))
